
  // $(document).ready(function() {
    // $("#chart_div").panzoom();

 //    $('#chart_div').on('mousedown touchstart', 'a', function( e ) {
	//     e.stopImmediatePropagation();
	// });

    // $("#chart_div").on('mousewheel.focal', function( e ) {
    //     e.preventDefault();
    //     var delta = e.delta || e.originalEvent.wheelDelta;
    //     var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
    //     $("#chart_div").panzoom('zoom', zoomOut, {
    //       increment: 0.1,
    //       animate: false,
    //       focal: e
    //     });
    //   });

  // });

odoo.define('family.tree', function (require) {
    var utils = require('web.utils');
    var Model = require('web.Model');
    var Session = require('web.Session');

	var partners = new Model('res.partner');
	var session = new Session();
	console.log(session);
	// console.log(Users);
	$.ajax({
	    type: "GET",
	    url: "/api/partners/chart", // URL of OpenERP Handler
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 

	    }, // error 
	    // script call was successful 
	    // data contains the JSON values returned by OpenERP 
	    success: function(response){
	    	response = JSON.parse(response);
	    	var members = [];
	    	for (var i=0;i<response.length;i++){
				var r = response[i];
				if (r['parent'] || (r['children'] && r['children'].length)) {
					for (var j=0;j<response.length;j++){
						var p = response[j];
						if ((r['spouse'] && r['spouse']['id'] == p['id']) || (p['spouse'] && p['spouse']['id'] == r['id'])) {
							r['spouse'] = p
						}
					}
					members.push(r);
				}
	    	}

	    	console.log(response);
	    	console.log(members);

			google.charts.load('current', {packages:["orgchart"]});
			google.charts.setOnLoadCallback(drawChart);

			function drawChart() {
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Name');
				data.addColumn('string', 'Parent');
				data.addColumn('string', 'ToolTip');

				// For each orgchart box, provide the name, manager, and tooltip to show.
				var rows = [];
				for (var i=0;i<members.length;i++){
					var r = members[i];
					var value_name = ''
					var value_parent = ''
					var value_tooltip = ''

					var dob = r['dob']? r['dob'] : '';
					var dod = r['dod']? r['dod'] : '';
					var death_class = r['dod']? 'death' : ''; 
					var added_title = r['dod']? 'Alm. ' : ''; 
					
					if (r['spouse']) {
						var spouse_dob = r['spouse']['dob']? r['spouse']['dob'] : '';
						var spouse_dod = r['spouse']['dod']? r['spouse']['dod'] : '';
						var spouse_death_class = r['spouse']['dod']? 'death' : ''; 
						var spouse_added_title = r['spouse']['dod']? 'Alm. ' : ''; 
						value_name = {
							v: r['name'], 
							f: '<a href="/open/partners/'+r['id']+'"><div class="spouse '+r['gender']+' '+death_class+'">'
									+'<img src="'+session['server']+'/web/image?model=res.partner&id='+r['id']+'&field=image"/>'
									+'<h5>'+added_title+r['name']+'</b></h5>'
									+'<p>'+dob+'</p>'
									+'<p>'+dod+'</p>'
								+'</div></a>'

								+'<a href="/open/partners/'+r['spouse']['id']+'"><div class="spouse '+r['spouse']['gender']+' '+spouse_death_class+'">'
									+'<img src="'+session['server']+'/web/image?model=res.partner&id='+r['spouse']['id']+'&field=image"/>'
									+'<h5>'+spouse_added_title+r['spouse']['name']+'</h5>'
									+'<p>'+spouse_dob+'</p>'
									+'<p>'+spouse_dod+'</p>'
								+'</div></a>'
						};
					}
					else 
						value_name = {
							v: r['name'], 
							f: '<a href="/open/partners/'+r['id']+'"><div class="'+r['gender']+' '+death_class+'">'
									+'<img src="'+session['server']+'/web/image?model=res.partner&id='+r['id']+'&field=image"/>'
									+'<h5>'+added_title+r['name']+'</h5>'
									+'<p>'+dob+'</p>'
									+'<p>'+dod+'</p>'
								+'</div></a>'
						};

					if (r['parent'])
						value_parent = r['parent']['name']

					rows.push([value_name, value_parent, value_tooltip]);
				}
				data.addRows(rows)
				// data.addRows([
				//   [{v:'Mike', f:'Mike<div style="color:red; font-style:italic">President</div>'},
				//    '', 'The President'],
				//   [{v:'Jim', f:'Jim<div style="color:red; font-style:italic">Vice President</div>'},
				//    'Mike', 'VP'],
				//   ['Alice', 'Mike', ''],
				//   ['Bob', 'Jim', 'Bob Sponge'],
				//   ['Carol', 'Bob', '']
				// ]);

				// Create the chart.
				var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
				// Draw the chart, setting the allowHtml option to true for the tooltips.
				chart.draw(data, {allowHtml:true, allowCollapse:true});
			}
	    } // success
	 }); // ajax


});