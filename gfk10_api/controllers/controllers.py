# -*- coding: utf-8 -*-
from odoo import http

import functools
import logging

import json
import time
import base64

import werkzeug.utils
from werkzeug.exceptions import BadRequest

from odoo import api, http, SUPERUSER_ID, _
from odoo.http import request
from odoo import registry as registry_get
from odoo.exceptions import AccessDenied
from odoo.addons.auth_signup.models.res_users import SignupError

from odoo import fields, models
import httplib
import functools
import hashlib
import logging
import os
from ast import literal_eval
try:
    import simplejson as json
except ImportError:
    import json

import werkzeug.wrappers

from odoo.http import request, OpenERPSession
from odoo.modules.registry import RegistryManager
from psycopg2.extensions import ISOLATION_LEVEL_READ_COMMITTED
import re
from datetime import datetime, timedelta

class Family(http.Controller):

    def validate_params(self, dictionary, params):
        res = True
        for param in params :
            if param not in dictionary :
                res = False
                return res
        return res

    def error_response(self, status, error, error_descrip):
        return werkzeug.wrappers.Response(
            status = status,
            content_type = 'application/json; charset=utf-8',
            #headers = None,
            response = json.dumps({
                'error':         error,
                'error_descrip': error_descrip,
            }),
        )

    def check_session_id(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            
            if not request.session.uid :
                return self.error_response(401, 'Unauthorized', 'Unauthorized')
            else :
                request.uid = request.session.uid
            
            # The code, following the decorator
            return func(self, *args, **kwargs)
        return wrapper

    @http.route('/api/login', type="http", auth="none", methods=['POST'], csrf=False, cors='*')
    def login(self, *args, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        res = {}
        User = request.env['res.users'].sudo()
        Partner = request.env['res.partner'].sudo()

        if jdata :
            login = jdata.get('login', False)
            password = jdata.get('password', False)
            db = request.db
            
            uid = request.session.authenticate(db, login, password)

            if uid :
                request.uid = uid

                user = User.search([('id','=',uid)])
                partner = user.partner_id
                res = { 
                    'id' : uid,
                    'name' : partner.name, 
                    'access_token' : request.env['ir.http'].session_info()['session_id'],
                    'username' : login,
                    # 'password' : password,

                    'session_id' : request.env['ir.http'].session_info()['session_id'],
                    'street' : partner.street or "",
                    'house_number' : partner.street or "",
                    'email' : partner.email or "",
                    'phone' : partner.phone or "",

                    'zip' : partner.zip or "",
                    }

            else :
                return json.dumps({
                    'error': _('The email or password is incorrect.'), 
                    'title': _('Login')
                    })
            
        return json.dumps(res)

    @http.route('/family/tree', type="http", auth='public', website=True, cors="*")
    # @check_session_id
    def family_tree(self, **kw):
        return request.render('family.tree')

    @http.route('/api/partners/chart', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_partners_chart(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.search([('id','=', request.uid)])
        partner = user.partner_id

        Partner = request.env['res.partner'].sudo()
        partners = Partner.search([('is_company', '=', False)], order="dob asc, id asc")
        
        
        res = []
        for record in partners :
            vals = {
                'id'   : record.id,
                'name' : record.name,
                'gender' : record.gender,
                'dob' : record.dob,
                'dod' : record.dod,
                'subtitle' : "",
                # 'subtitle' : "Anak "+record.parent.name if record.parent else "",
                'image' : request.env['ir.config_parameter'].sudo().get_param('web.base.url')+'/web/image?model=res.partner&id='+str(record.id)+'&field=image',
            }
            vals['children'] = []
            if record.parent :
                vals['parent'] = {
                    'id' : record.parent.id,
                    'name' : record.parent.name,
                }

            if record.spouse :
                vals['spouse'] = {
                    'id' : record.spouse.id,
                    'name' : record.spouse.name,
                }


            for child in record.children :
                vals['children'].append({
                    'id' : child.id,
                    'name' : child.name,
                })
            res.append(vals)
        
        return json.dumps(res)

    @http.route('/api/partners', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_partners(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.search([('id','=', request.uid)])
        partner = user.partner_id

        Partner = request.env['res.partner'].sudo()
        partners = Partner.search([('gfk', '=', True)], order="name asc")#order="dob asc, id asc")
        
        
        res = []
        for record in partners :
            vals = {
                'id'   : record.id,
                'name' : record.name,
                'latitude' : record.latitude,
                'longitude' : record.longitude,
                'gender' : record.gender,
                'dob' : record.dob,
                'dod' : record.dod,
                'subtitle' : "",
                # 'subtitle' : "Anak "+record.parent.name if record.parent else "",
                'image' : request.env['ir.config_parameter'].sudo().get_param('web.base.url')+'/web/image?model=res.partner&id='+str(record.id)+'&field=image',
            }
            vals['children'] = []
            if record.parent :
                vals['parent'] = {
                    'id' : record.parent.id,
                    'name' : record.parent.name,
                }

            if record.spouse :
                vals['spouse'] = {
                    'id' : record.spouse.id,
                    'name' : record.spouse.name,
                }


            for child in record.children :
                vals['children'].append({
                    'id' : child.id,
                    'name' : child.name,
                })
            res.append(vals)
        
        return json.dumps(res)

    @http.route('/api/genders', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_genders(self, **kw):
        res = [
            {
                'value' : 'm',
                'name' : 'Laki-laki',
                'subtitle' : "",
            },
            {
                'value' : 'f',
                'name' : 'Perempuan',
                'subtitle' : "",
            }
        ]
        
        return json.dumps(res)

    @http.route('/api/selections/<field>', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_selections(self, field, **kw):
        Partner = request.env['res.partner'].sudo()
        if isinstance(Partner._fields[field], fields.Selection) :
            res = []
            try : 
                selection_dict = dict(Partner._fields[field].selection)
                for key in selection_dict :
                    res.append({
                        'name' : selection_dict.get(key),
                        'value' : key,
                        'subtitle' : "",
                    })
            except :
                res = []
        else :
            res = []
        
        return json.dumps(res)

    @http.route('/api/list/<model>', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_list(self, model, **kw):
        res = request.env[model].sudo().search_read([],['id', 'name'])
        return json.dumps(res)

    @http.route('/api/partners/<int:partner_id>', methods=['GET'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def get_partner(self, partner_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.search([('id','=', request.uid)])
        partner = user.partner_id

        Partner = request.env['res.partner'].sudo()
        partner = Partner.browse(partner_id)
        
        res = partner.read([])
        if res :
            res = res[0]
            list_deleted_key = []
            for key in res :
                if 'image' in key :
                    # list_deleted_key.append(key)
                    res[key] = request.env['ir.config_parameter'].sudo().get_param('web.base.url')+'/web/image?model=res.partner&id='+str(partner_id)+'&field=image',
                if isinstance(Partner._fields[key], fields.Many2one) :
                    if isinstance(res[key], tuple) :
                        res[key] = {
                            "id" : res[key][0],
                            "name" : res[key][1],
                        }
                    if res[key] == False :
                        res[key] = {
                            "id" : 0,
                            "name" : '',
                        }
                if isinstance(Partner._fields[key], fields.Char) or isinstance(Partner._fields[key], fields.Text) or isinstance(Partner._fields[key], fields.Date) :
                    if res[key] == False :
                        res[key] = ""
                if isinstance(Partner._fields[key], fields.Selection) :
                    if res[key] and Partner._fields[key].selection:
                        try : 
                            selection_dict = dict(Partner._fields[key].selection)
                            res[key] = {
                                "value" : res[key],
                                "name" : selection_dict.get(res[key]),
                            }
                        except :
                            res[key] = { "value" : "", "name" : "" }
                    else :
                        res[key] = { "value" : "", "name" : "" }

            for key in list_deleted_key :
                del res[key]
         
        return json.dumps(res)

    @http.route('/api/partners/create', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def create_partner(self, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.search([('id','=', request.uid)])
        partner = user.partner_id

        Partner = request.env['res.partner'].sudo()
        
        res = {}
        if jdata and self.validate_params(jdata, ['name']) :
            jdata['gfk'] = True
            record = Partner.create(jdata)
        
        return json.dumps({"id" : record.id})

    @http.route('/api/partners/update/<int:partner_id>', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def update_partner(self, partner_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}

        User = request.env['res.users'].sudo()
        user = User.search([('id','=', request.uid)])
        partner = user.partner_id

        Partner = request.env['res.partner'].sudo()
        update_partner = Partner.browse(partner_id);
        
        res = {}
        if jdata and self.validate_params(jdata, ['name']) :
            jdata['gfk'] = True
            record = update_partner.write(jdata)
        
        return json.dumps({"id" : update_partner.id})

    @http.route('/api/partners/image/<int:partner_id>', methods=['POST'], type="http", auth="none", csrf=False, cors='*')
    # @check_session_id
    def upload_image_partner(self, partner_id, **kw):
        try:
            jdata = json.loads(request.httprequest.stream.read())
        except:
            jdata = {}


        Partner = request.env['res.partner'].sudo()
        update_partner = Partner.browse(partner_id);
        image = kw.get('pic', False)
        if image :
            update_partner.image = base64.encodestring(image.read())
        
        return json.dumps({})

