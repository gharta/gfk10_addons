# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Partner(models.Model):
    _inherit = 'res.partner'

    dob = fields.Date('Date of Birth')
    dod = fields.Date('Date of Death')
    parent = fields.Many2one('res.partner', 'Parent')
    spouse = fields.Many2one('res.partner', 'Spouse')
    children = fields.One2many('res.partner', 'parent', string='Children')
    gender = fields.Selection([('m', 'Male'), ('f', 'Female')], string='Gender')

    # -*- coding: utf-8 -*-

from odoo import models, fields, api
from openerp.exceptions import ValidationError
# -*- coding: utf-8 -*-
from odoo import models,fields,api

class ResCompany(models.Model):
    _inherit = "res.company"
    
    city_id = fields.Many2one('res.state.city', 'Kota')
    country_id = fields.Many2one(default=101)
    
class Country(models.Model):
    _inherit = "res.country"
    
    state_ids = fields.One2many('res.country.state','country_id',"Propinsi")
    
class State(models.Model):
    _inherit = "res.country.state"
    
    code = fields.Char(size=255)
    city_ids = fields.One2many("res.state.city","state_id", "Kota/Kabupaten")
    
class City(models.Model):
    _name = "res.state.city"
    
    name = fields.Char("Nama Kota/Kabupaten")
    kabupaten = fields.Boolean('Kabupaten')
    code = fields.Char("Kode Kota")
    state_id = fields.Many2one('res.country.state', 'Propinsi')
    kecamatan_ids = fields.One2many('res.city.kecamatan', 'city_id','Kecamatan')
    
#     _sql_constraints = [('code_unique', 'unique(code)', "Kode kota harus unik")]
class Kecamatan(models.Model):
    _name = 'res.city.kecamatan'
    
    name = fields.Char('Nama Kecamatan')
    code = fields.Char('Kode Kecamatan')
    city_id = fields.Many2one('res.state.city', 'Kota/Kabupaten')
    kelurahan_ids = fields.One2many('res.kecamatan.kelurahan', 'kecamatan_id','Kelurahan')
    
class Kelurahan(models.Model):
    _name = 'res.kecamatan.kelurahan'
    
    name = fields.Char('Nama Kelurahan/Desa')
    zip = fields.Char('Kodepos')
    desa = fields.Boolean('Desa')
    kecamatan_id = fields.Many2one('res.city.kecamatan','Kecamatan')
    
    
# class Users(models.Model):
#     _inherit = "res.users"
    
#     leader_id = fields.Many2one('res.users', 'Team Leader')
#     team_ids = fields.One2many('res.users', 'leader_id', 'Team')

class Building(models.Model):
    _name = "building.building"

    name = fields.Char("Name")
    street = fields.Char("Street")
    kelurahan_id = fields.Many2one('res.kecamatan.kelurahan','Kecamatan')
    zip = fields.Char("Kode Pos")
    active = fields.Boolean("Active")

#questionnaire

class UCQ(models.Model):
    _inherit = "res.partner"
    
    latitude = fields.Float("Latitude", default=-34)
    longitude = fields.Float("Longitude", default=151)
    questioner_code = fields.Char("Questioner Code")
    # UC Area
    state_id = fields.Many2one("res.country.state", "Propinsi")
    dati = fields.Selection([('city','Kota'),('district', 'Kabupaten')], "Dati")
    city_id = fields.Many2one('res.state.city', 'Kota/Kabupaten')
    kecamatan_id = fields.Many2one('res.city.kecamatan','Kecamatan')
    kelurahan_id = fields.Many2one('res.kecamatan.kelurahan','Desa/Kelurahan')
    zip = fields.Char("Kode Pos")
    
    gfk = fields.Boolean()

    # Administration
    interviewer_id = fields.Many2one('res.users', 'Interviewer')
    # leader_id = fields.Many2one('res.users', 'Team Leader', related="interviewer_id.leader_id", store=1, readonly=1)

    # Shop Information
    name = fields.Char(size=32)
    state = fields.Selection([('draft', 'Draft'),('waiting', 'Waiting Approval'),('revise', 'Revise'),('approved','Approved')], 'State', default='draft')
    building_name_id = fields.Many2one('building.building',"Building Name")
    building_block = fields.Char("Blok")
    building_block_no = fields.Char("No.") 
    building_floor = fields.Char("Floor")
    street = fields.Char("Street", size=64)
    block = fields.Char("Blok")
    block_no = fields.Char("No.")
    phone = fields.Char("Phone No.")
    store_loc_type = fields.Selection([("street", "Street"),("mall", "Mall"),("market", "Market"),("shop", "Shop"),("sis", "S.I.S")], "Store Location")
    store_length = fields.Integer("Panjang")
    store_width = fields.Integer("Lebar")
    shop_size = fields.Selection([("xl", "XL"), ("l", "L"), ("m", "M"), ("s", "S"), ("xs", "XS")], "Shop Size")
    rent_fee = fields.Float("Rent Fee", help="Rent Fee per Year")
    visiting_evidence = fields.Selection([("name_card", "Name Card"),("stamp", "Stamp"),("receipt", "Receipt"),("brochure", "Brochure"),("photo", "Photo")], "Visiting Evidence")
    age_of_store = fields.Float("Age of Store")
    respondent_name = fields.Char("Respondent Name", size=32)
    shop_status = fields.Selection([("panel","Panel"),("non_panel","Non-Panel")], "Shop Status")
    longitude_start = fields.Float()
    latitude_start = fields.Float()
    longitude_end = fields.Float()
    latitude_end = fields.Float()
    dt_start = fields.Datetime()
    dt_end = fields.Datetime()
    gsnr = fields.Char("GSNR")
    check_out_counter = fields.Char("Check Out Counter")
    software_pembukuan_pos = fields.Selection([("yes","Yes"),("no","No")], "Software Pembukuan (POS)")
    merk_tunggal = fields.Char("Merk Tunggal")
    shop_front = fields.Selection([("store","Store"),("counter","Counter")], "Shop Front")
    
    no_floor = fields.Char("No. Of Floor")
    no_employee = fields.Char("No. Of Employee")
    shop_turnover = fields.Char("Shop Turnover")
    delivery_vehicle = fields.Char("Delivery Vehicle")




    # Keterangan Tambahan
    additional_info = fields.Text("Keterangan Tambahan")

    # persentase display
    audio_visual_prcntg = fields.Float("Audio Visual Percentage")
    home_entertainment_prcntg = fields.Float("Home Entertainment Percentage")
    sda_prcntg = fields.Float("SDA Percentage")
    mda_prcntg = fields.Float("MDA Percentage")
    photo_prcntg = fields.Float("Photo Percentage")
    it_prcntg = fields.Float("IT Percentage")
    telco_prcntg = fields.Float("Telecom Percentage")
    auto_prcntg = fields.Float("Auto Percentage")
    lightning_prcntg = fields.Float("Lightning Percentage")
    other_prcntg = fields.Float("Other Percentage")
    total_main_prcntg = fields.Float("Total Percentage")

    it_dpc_prcntg = fields.Float("IT (DPC/PPC) Percentage")
    it_peripheral_prcntg = fields.Float("IT Peripherals Percentage")
    it_accessories_prcntg = fields.Float("IT Accessories Percentage")
    it_component_prcntg = fields.Float("IT Component Percentage")
    it_networking_prcntg = fields.Float("IT Networking Percentage")
    it_other_prcntg = fields.Float("IT Other Percentage")
    total_it_prcntg = fields.Float("Total Percentage")

    # Shop Type
    electrical_type = fields.Selection(
            [
                ('avs','AVS (Audio Visual Store)'),
                ('ges','GES (General Electro Store)'),
                ('wgs','WGS (White Good Specialist)'),
                ('cbs','CBS (Combi Shop)'),
                ('sda','SDA (Small Domestic App Sp)'),
                ('air','AIR (Air Condition Specialist)'),
                ('im','IM (Iron Mongers)'),
                ('glw','GLW (Households)'),
                ('ls','LS (Lightning Sp.)'),
            ], "Electrical"
        )
    audio_type = fields.Selection(
            [
                ('cgr','CGR (Car Garages)'),
                ('cts','CTS (Car Tyre Specialist)'),
                ('ego','EGO (Engine Oil Specialist)'),
            ],"Audio"
        )
    telco_type = fields.Selection(
            [
                ('tcs','TCS (Telecom Specialist)'),
                ('tcs2','TCS2 (Telecom Specialist (2nd Specialist))'),
                ('tca','TCA (Telecom Accessories Sp)'),
                ('vsp','VSP (Voucher Specialist (Sales MOB))'),
                ('mbs','MBS (Mobile Broadband Specialist)'),
            ],"Telecom"
        )
    computer_type = fields.Selection(
            [
                ('its','IT Specialist'),
                ('iass','IT Assembled Sp'),
                ('ibrd','IT Branded Specialist'),
                ('inbs','IT Notebook Specialist'),
                ('ipps','IT Peripherals/Accessories Sp'),
                ('prt','Printer Specialist'),
                ('wbk','Webbook Specialist'),
                ('gdt','Gadget Store')
            ], "Computer"
        )
    photo_type = fields.Selection(
            [
                ('pho','Photo Specialist'),
                ('ml','Minilabs'),
            ], "Photo"
        )

    # Product Brand and Display
    # Audio Visual
    av_uhd_tv_lg = fields.Boolean("UHD TV (lg)")
    av_uhd_tv_sharp = fields.Boolean("UHD TV (sharp)")
    av_uhd_tv_panasonic = fields.Boolean("UHD TV (panasonic)")
    av_uhd_tv_polytron = fields.Boolean("UHD TV (polytron)")
    av_uhd_tv_samsung = fields.Boolean("UHD TV (samsung)")
    av_uhd_tv_sony = fields.Boolean("UHD TV (sony)")
    av_uhd_tv_gmc = fields.Boolean("UHD TV (gmc)")
    av_uhd_tv_others = fields.Char("UHD TV (others)")
    av_uhd_tv_qty = fields.Float("UHD TV (Total Unit)")
    av_p_tv_lg = fields.Boolean("PTV (TV LCD/LED/PLASMA) (lg)")
    av_p_tv_sharp = fields.Boolean("PTV (TV LCD/LED/PLASMA) (sharp)")
    av_p_tv_panasonic = fields.Boolean("PTV (TV LCD/LED/PLASMA) (panasonic)")
    av_p_tv_polytron = fields.Boolean("PTV (TV LCD/LED/PLASMA) (polytron)")
    av_p_tv_samsung = fields.Boolean("PTV (TV LCD/LED/PLASMA) (samsung)")
    av_p_tv_sony = fields.Boolean("PTV (TV LCD/LED/PLASMA) (sony)")
    av_p_tv_gmc = fields.Boolean("PTV (TV LCD/LED/PLASMA) (gmc)")
    av_p_tv_others = fields.Char("PTV (TV LCD/LED/PLASMA) (others)")
    av_p_tv_qty = fields.Float("PTV (TV LCD/LED/PLASMA) (Total Unit)")
    av_ahs_lg = fields.Boolean("AHS (Mini Midi/Home Theater) (lg)")
    av_ahs_sharp = fields.Boolean("AHS (Mini Midi/Home Theater) (sharp)")
    av_ahs_panasonic = fields.Boolean("AHS (Mini Midi/Home Theater) (panasonic)")
    av_ahs_polytron = fields.Boolean("AHS (Mini Midi/Home Theater) (polytron)")
    av_ahs_samsung = fields.Boolean("AHS (Mini Midi/Home Theater) (samsung)")
    av_ahs_sony = fields.Boolean("AHS (Mini Midi/Home Theater) (sony)")
    av_ahs_gmc = fields.Boolean("AHS (Mini Midi/Home Theater) (gmc)")
    av_ahs_others = fields.Char("AHS (Mini Midi/Home Theater) (others)")
    av_ahs_qty = fields.Float("AHS (Mini Midi/Home Theater) (Total Unit)")
    av_headphone = fields.Boolean("Headphone")
    av_radio_recorder_qty = fields.Float("Radio/Recorder (Kaset) Player")
    av_player = fields.Boolean("MP3/MP4 Player")
    av_clock_radio = fields.Boolean("Clock Radio")
    av_speaker = fields.Boolean("Speaker (Speaker Besar/Salon)")
    av_sound_bar_qty = fields.Float("Sound Bar/Soundplate/Lap341 (Total Unit)")
    av_crt_tv_qty = fields.Float("CRT-TV (TV Tabung) (Total Unit)")
    av_camcorder_qty = fields.Float("Camcorder (Handycam) (Total Unit)")
    av_dvd_player = fields.Boolean("DVD/Blue-Ray Player")
    av_car_gps = fields.Boolean("Car Navigation (GPS)")
    av_projector = fields.Boolean("Projector")
    av_others = fields.Boolean('Others (Total Unit)')

    # Lightning
    lit_cfl_omi = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (omi)")
    lit_cfl_philips = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (philips)")
    lit_cfl_hannochs = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (hannochs)")
    lit_cfl_panasonic = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (panasonic)")
    lit_cfl_osram = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (osram)")
    lit_cfl_atama = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (atama)")
    lit_cfl_others = fields.Char("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (Others)")
    lit_cfl_qty = fields.Boolean("Compact Flurescent Lamp (CFL) (Lampu Hemat Energi) (Total Unit)")
    lit_luminnaire_omi = fields.Boolean("Luminnaire (Downlight & Batten) (omi)")
    lit_luminnaire_philips = fields.Boolean("Luminnaire (Downlight & Batten) (philips)")
    lit_luminnaire_hannochs = fields.Boolean("Luminnaire (Downlight & Batten) (hannochs)")
    lit_luminnaire_panasonic = fields.Boolean("Luminnaire (Downlight & Batten) (panasonic)")
    lit_luminnaire_osram = fields.Boolean("Luminnaire (Downlight & Batten) (osram)")
    lit_luminnaire_atama = fields.Boolean("Luminnaire (Downlight & Batten) (atama)")
    lit_luminnaire_others = fields.Char("Luminnaire (Downlight & Batten)")
    lit_luminnaire = fields.Boolean("Luminnaire (Downlight & Batten) (Total Unit)")
    lit_tl_omi = fields.Boolean("Fluorescent Lamp (Lampu TL) (omi)")
    lit_tl_philips = fields.Boolean("Fluorescent Lamp (Lampu TL) (philips)")
    lit_tl_hannochs = fields.Boolean("Fluorescent Lamp (Lampu TL) (hannochs)")
    lit_tl_panasonic = fields.Boolean("Fluorescent Lamp (Lampu TL) (panasonic)")
    lit_tl_osram = fields.Boolean("Fluorescent Lamp (Lampu TL) (osram)")
    lit_tl_atama = fields.Boolean("Fluorescent Lamp (Lampu TL) (atama)")
    lit_tl_others = fields.Char("Fluorescent Lamp (Lampu TL) (others)")
    lit_tl = fields.Boolean("Fluorescent Lamp (Lampu TL) (Total Unit)")
    lit_led_omi = fields.Boolean("LED Lamp (omi)")
    lit_led_philips = fields.Boolean("LED Lamp (philips)")
    lit_led_hannochs = fields.Boolean("LED Lamp (hannochs)")
    lit_led_panasonic = fields.Boolean("LED Lamp (panasonic)")
    lit_led_osram = fields.Boolean("LED Lamp (osram)")
    lit_led_atama = fields.Boolean("LED Lamp (atama)")
    lit_led_others = fields.Char("LED Lamp (others)")
    lit_led = fields.Boolean("LED Lamp (Total Unit)")
    lit_halogen = fields.Boolean("LED Lamp")
    lit_incandescent = fields.Boolean("Incandescent Lamp (Lampu Pijar/Bohlam)")
    lit_material = fields.Boolean("Electrical Material (Kabel, Stop Kontak, Saklar)")
    lit_others = fields.Boolean('Others (Total Unit)')

    # MDA dan SDA
    msda_washing_double_lg = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (lg)")
    msda_washing_double_sharp = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (sharp)")
    msda_washing_double_panasonic = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (panasonic)")
    msda_washing_double_polytron = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (polytron)")
    msda_washing_double_samsung = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (samsung)")
    msda_washing_double_aqua = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (aqua)")
    msda_washing_double_rinnai = fields.Boolean("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (rinnai)")
    msda_washing_double_others = fields.Char("Washing Machine Double Tube (Mesin Cuci Dua Tabung) (others)")
    msda_washing_double_qty = fields.Float("Washing Machine Double Tube (Mesin Cuci Dua Tabung)")
    msda_washing_single_lg = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (lg)")
    msda_washing_single_sharp = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (sharp)")
    msda_washing_single_panasonic = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (panasonic)")
    msda_washing_single_polytron = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (polytron)")
    msda_washing_single_samsung = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (samsung)")
    msda_washing_single_aqua = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (aqua)")
    msda_washing_single_rinnai = fields.Boolean("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (rinnai)")
    msda_washing_single_others = fields.Char("Washing Machine Single Tube (Mesin Cuci Satu Tabung) (Others)")
    msda_washing_single_qty = fields.Float("Washing Machine Single Tube (Mesin Cuci Satu Tabung)")
    msda_washing_front_lg = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (lg)")
    msda_washing_front_sharp = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (sharp)")
    msda_washing_front_panasonic = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (panasonic)")
    msda_washing_front_polytron = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (polytron)")
    msda_washing_front_samsung = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (samsung)")
    msda_washing_front_aqua = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (aqua)")
    msda_washing_front_rinnai = fields.Boolean("Washing Machine (Mesin Cuci) Front Loading (rinnai)")
    msda_washing_front_others = fields.Char("Washing Machine (Mesin Cuci) Front Loading (Others)")
    msda_washing_front_qty = fields.Float("Washing Machine (Mesin Cuci) Front Loading")
    msda_ac_lg = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (lg)")
    msda_ac_sharp = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (sharp)")
    msda_ac_panasonic = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (panasonic)")
    msda_ac_polytron = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (polytron)")
    msda_ac_samsung = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (samsung)")
    msda_ac_aqua = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (aqua)")
    msda_ac_rinnai = fields.Boolean("Air Conditioner (AC Split/Window/Standing) (rinnai)")
    msda_ac_others = fields.Char("Air Conditioner (AC Split/Window/Standing) (Others)")
    msda_ac_qty = fields.Float("Air Conditioner (AC Split/Window/Standing) (Total Unit)")
    msda_cooling_two_lg = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (lg)")
    msda_cooling_two_sharp = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (sharp)")
    msda_cooling_two_panasonic = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (panasonic)")
    msda_cooling_two_polytron = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (polytron)")
    msda_cooling_two_samsung = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (samsung)")
    msda_cooling_two_aqua = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (aqua)")
    msda_cooling_two_rinnai = fields.Boolean("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (rinnai)")
    msda_cooling_two_others = fields.Char("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (Others)")
    msda_cooling_two_qty = fields.Float("Cooling 2 Door ++ (Kulkas 2 Pintu ++) (Total Unit)")
    msda_cooling_sbs_lg = fields.Boolean("Cooling SBS (Kulkas Side By Side) (lg)")
    msda_cooling_sbs_sharp = fields.Boolean("Cooling SBS (Kulkas Side By Side) (sharp)")
    msda_cooling_sbs_panasonic = fields.Boolean("Cooling SBS (Kulkas Side By Side) (panasonic)")
    msda_cooling_sbs_polytron = fields.Boolean("Cooling SBS (Kulkas Side By Side) (polytron)")
    msda_cooling_sbs_samsung = fields.Boolean("Cooling SBS (Kulkas Side By Side) (samsung)")
    msda_cooling_sbs_aqua = fields.Boolean("Cooling SBS (Kulkas Side By Side) (aqua)")
    msda_cooling_sbs_rinnai = fields.Boolean("Cooling SBS (Kulkas Side By Side) (rinnai)")
    msda_cooling_sbs_others = fields.Char("Cooling SBS (Kulkas Side By Side) (Others)")
    msda_cooling_sbs_qty = fields.Float("Cooling SBS (Kulkas Side By Side) (Total Unit)")
    msda_cooling_one_lg = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (lg)")
    msda_cooling_one_sharp = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (sharp)")
    msda_cooling_one_panasonic = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (panasonic)")
    msda_cooling_one_polytron = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (polytron)")
    msda_cooling_one_samsung = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (samsung)")
    msda_cooling_one_aqua = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (aqua)")
    msda_cooling_one_rinnai = fields.Boolean("Cooling 1 Door (Kulkas 1 Pintu) (rinnai)")
    msda_cooling_one_others = fields.Char("Cooling 1 Door (Kulkas 1 Pintu) (Others)")
    msda_cooling_one_qty = fields.Float("Cooling 1 Door (Kulkas 1 Pintu) (Total Unit)")
    msda_freestd_hobs_lg = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (lg)")
    msda_freestd_hobs_sharp = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (sharp)")
    msda_freestd_hobs_panasonic = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (panasonic)")
    msda_freestd_hobs_polytron = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (polytron)")
    msda_freestd_hobs_samsung = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (samsung)")
    msda_freestd_hobs_aqua = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (aqua)")
    msda_freestd_hobs_rinnai = fields.Boolean("Freestd. Hobs (Kompor Gas/Listrik) (rinnai)")
    msda_freestd_hobs_others = fields.Char("Freestd. Hobs (Kompor Gas/Listrik) (Others)")
    msda_freestd_hobs_qty = fields.Float("Freestd. Hobs (Kompor Gas/Listrik) (Total Unit)")
    msda_showcase_lg = fields.Boolean("Showcase (Baverage Display) (lg)")
    msda_showcase_sharp = fields.Boolean("Showcase (Baverage Display) (sharp)")
    msda_showcase_panasonic = fields.Boolean("Showcase (Baverage Display) (panasonic)")
    msda_showcase_polytron = fields.Boolean("Showcase (Baverage Display) (polytron)")
    msda_showcase_samsung = fields.Boolean("Showcase (Baverage Display) (samsung)")
    msda_showcase_aqua = fields.Boolean("Showcase (Baverage Display) (aqua)")
    msda_showcase_rinnai = fields.Boolean("Showcase (Baverage Display) (rinnai)")
    msda_showcase_others = fields.Char("Showcase (Baverage Display) (Others)")
    msda_showcase_qty = fields.Float("Showcase (Baverage Display) (Total Unit)")
    msda_microwave_oven_lg = fields.Boolean("Microwave Oven (lg)")
    msda_microwave_oven_sharp = fields.Boolean("Microwave Oven (sharp)")
    msda_microwave_oven_panasonic = fields.Boolean("Microwave Oven (panasonic)")
    msda_microwave_oven_polytron = fields.Boolean("Microwave Oven (polytron)")
    msda_microwave_oven_samsung = fields.Boolean("Microwave Oven (samsung)")
    msda_microwave_oven_aqua = fields.Boolean("Microwave Oven (aqua)")
    msda_microwave_oven_rinnai = fields.Boolean("Microwave Oven (rinnai)")
    msda_microwave_oven_others = fields.Char("Microwave Oven (Others)")
    msda_microwave_oven_qty = fields.Float("Microwave Oven (Total Unit)")
    msda_cooking_qty = fields.Float("Cooking")
    msda_cooker_hoods_qty = fields.Float("Cooker Hoods (Penghisap Asap)")
    msda_builtin_hobs_qty = fields.Float("Built in Hobs (Kompor Tanam)")
    msda_water_pump = fields.Char("Water Pump (Pompa Air) (Total Unit)")
    msda_others = fields.Char('Others')
    msda_others_qty = fields.Boolean('Others (Total Unit)')
    
    # SDA
    sda_irons_kirin = fields.Boolean('Irons (Setrika) (kirin) (kirin)')
    sda_irons_maspion = fields.Boolean('Irons (Setrika) (maspion) (maspion)')
    sda_irons_philips = fields.Boolean('Irons (Setrika) (philips) (philips)')
    sda_irons_panasonic = fields.Boolean('Irons (Setrika) (panasonic) (panasonic)')
    sda_irons_miyako = fields.Boolean('Irons (Setrika) (miyako) (miyako)')
    sda_irons_yongma = fields.Boolean('Irons (Setrika) (yongma) (yongma)')
    sda_irons_cosmos = fields.Boolean('Irons (Setrika) (cosmos) (cosmos)')
    sda_irons_others = fields.Char('Irons (Setrika) (cosmos) (Others)')
    sda_irons_qty = fields.Float('Irons (Setrika) (cosmos) (Total Unit)')
    sda_food_processor_kirin = fields.Boolean('Food Processor/Blender/Mixer/Chopper (kirin)')
    sda_food_processor_maspion = fields.Boolean('Food Processor/Blender/Mixer/Chopper (maspion)')
    sda_food_processor_philips = fields.Boolean('Food Processor/Blender/Mixer/Chopper (philips)')
    sda_food_processor_panasonic = fields.Boolean('Food Processor/Blender/Mixer/Chopper (panasonic)')
    sda_food_processor_miyako = fields.Boolean('Food Processor/Blender/Mixer/Chopper (miyako)')
    sda_food_processor_yongma = fields.Boolean('Food Processor/Blender/Mixer/Chopper (yongma)')
    sda_food_processor_cosmos = fields.Boolean('Food Processor/Blender/Mixer/Chopper (cosmos)')
    sda_food_processor_others = fields.Char('Food Processor/Blender/Mixer/Chopper (Others)')
    sda_food_processor_qty = fields.Float('Food Processor/Blender/Mixer/Chopper (Total Unit)')
    sda_juicers_kirin = fields.Boolean('Juicers (kirin)')
    sda_juicers_maspion = fields.Boolean('Juicers (maspion)')
    sda_juicers_philips = fields.Boolean('Juicers (philips)')
    sda_juicers_panasonic = fields.Boolean('Juicers (panasonic)')
    sda_juicers_miyako = fields.Boolean('Juicers (miyako)')
    sda_juicers_yongma = fields.Boolean('Juicers (yongma)')
    sda_juicers_cosmos = fields.Boolean('Juicers (cosmos)')
    sda_juicers_others = fields.Char('Juicers (Others)')
    sda_juicers_qty = fields.Float('Juicers (Total Unit)')
    sda_rice_cooker_kirin = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (kirin)')
    sda_rice_cooker_maspion = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (maspion)')
    sda_rice_cooker_philips = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (philips)')
    sda_rice_cooker_panasonic = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (panasonic)')
    sda_rice_cooker_miyako = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (miyako)')
    sda_rice_cooker_yongma = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (yongma)')
    sda_rice_cooker_cosmos = fields.Boolean('Rice Cooker, Magic com/ Magic Jar (cosmos)')
    sda_rice_cooker_others = fields.Char('Rice Cooker, Magic com/ Magic Jar (Others)')
    sda_rice_cooker_qty = fields.Float('Rice Cooker, Magic com/ Magic Jar (Total Unit)')
    sda_vacuum_cleaner_kirin = fields.Boolean('Vacuum Cleaner (kirin)')
    sda_vacuum_cleaner_maspion = fields.Boolean('Vacuum Cleaner (maspion)')
    sda_vacuum_cleaner_philips = fields.Boolean('Vacuum Cleaner (philips)')
    sda_vacuum_cleaner_panasonic = fields.Boolean('Vacuum Cleaner (panasonic)')
    sda_vacuum_cleaner_miyako = fields.Boolean('Vacuum Cleaner (miyako)')
    sda_vacuum_cleaner_yongma = fields.Boolean('Vacuum Cleaner (yongma)')
    sda_vacuum_cleaner_cosmos = fields.Boolean('Vacuum Cleaner (cosmos)')
    sda_vacuum_cleaner_others = fields.Char('Vacuum Cleaner (Others)')
    sda_vacuum_cleaner_qty = fields.Float('Vacuum Cleaner (Total Unit)')
    sda_water_dispenser_kirin = fields.Boolean('Water Dispenser (kirin)')
    sda_water_dispenser_maspion = fields.Boolean('Water Dispenser (maspion)')
    sda_water_dispenser_philips = fields.Boolean('Water Dispenser (philips)')
    sda_water_dispenser_panasonic = fields.Boolean('Water Dispenser (panasonic)')
    sda_water_dispenser_miyako = fields.Boolean('Water Dispenser (miyako)')
    sda_water_dispenser_yongma = fields.Boolean('Water Dispenser (yongma)')
    sda_water_dispenser_cosmos = fields.Boolean('Water Dispenser (cosmos)')
    sda_water_dispenser_others = fields.Char('Water Dispenser (Others)')
    sda_water_dispenser_qty = fields.Float('Water Dispenser (Total Unit)')
    sda_air_treatment_qty = fields.Float('Air Treatment (Air Purifier) (Total Unit)')
    sda_shaver = fields.Char('Shaver (alat cukur listrik)')
    sda_filter_unilever = fields.Boolean('Water Filter (unilever)')
    sda_filter_others = fields.Char('Water Filter (others)')
    sda_filter_qty = fields.Boolean('Water Filter (Total Unit)')
    sda_filter_dispenser_unilever = fields.Boolean('Water Filter Dispenser (unilever)')
    sda_filter_dispenser_others = fields.Char('Water Filter Dispenser (others)')
    sda_filter_dispenser_qty = fields.Float('Water Filter Dispenser (Total Unit)')
    sda_filter_cartridge_qty = fields.Boolean('Water Filter Cartridge (Total Unit)')
    sda_water_heater_aqua = fields.Boolean('Water Heater (aqua)')
    sda_water_heater_gainbr = fields.Boolean('Water Heater (gainbr)')
    sda_water_heater_modena = fields.Boolean('Water Heater (modena)')
    sda_water_heater_paloma = fields.Boolean('Water Heater (paloma)')
    sda_water_heater_rinnai = fields.Boolean('Water Heater (rinnai)')
    sda_water_heater_wasser = fields.Boolean('Water Heater (wasser)')
    sda_water_heater_others = fields.Boolean('Water Heater (others)')
    sda_water_heater_qty = fields.Boolean('Water Heater (qty)')
    sda_others = fields.Boolean('Others (Total Unit)')

    # AUTO-CAR TYRE
    auto_car_tyre_dunlop = fields.Boolean('Car Tyre (Ban Mobil) (Dunlop)')
    auto_car_tyre_hankook = fields.Boolean('Car Tyre (Ban Mobil) (Hankook)')
    auto_car_tyre_goodyear = fields.Boolean('Car Tyre (Ban Mobil) (Goodyear)')
    auto_car_tyre_bridgestone = fields.Boolean('Car Tyre (Ban Mobil) (Bridgestone)')
    auto_car_tyre_accelera = fields.Boolean('Car Tyre (Ban Mobil) (Accelera)')
    auto_car_tyre_gtradial = fields.Boolean('Car Tyre (Ban Mobil) (GT Radial)')
    auto_car_tyre_achilles = fields.Boolean('Car Tyre (Ban Mobil) (Achilles)')
    auto_car_tyre_others = fields.Char('Car Tyre (Ban Mobil)')
    auto_car_tyre_qty = fields.Float('Car Tyre (Ban Mobil) (Total Unit)')

    # AUTO-CAR LUBRICANT
    auto_car_lubricant_engine_oil_car_shell = fields.Boolean('Engine Oil - Car (Oli Mobil) (Shell)')
    auto_car_lubricant_engine_oil_car_top1 = fields.Boolean('Engine Oil - Car (Oli Mobil) (Top 1)')
    auto_car_lubricant_engine_oil_car_castrol = fields.Boolean('Engine Oil - Car (Oli Mobil) (Castrol)')
    auto_car_lubricant_engine_oil_car_pertamina= fields.Boolean('Engine Oil - Car (Oli Mobil) (Pertamina)')
    auto_car_lubricant_engine_oil_car_motul = fields.Boolean('Engine Oil - Car (Oli Mobil) (Motul)')
    auto_car_lubricant_engine_oil_car_repsol = fields.Boolean('Engine Oil - Car (Oli Mobil) (Repsol)')
    auto_car_lubricant_engine_oil_car_mobil = fields.Boolean('Engine Oil - Car (Oli Mobil) (Mobil)')
    auto_car_lubricant_engine_oil_car_others = fields.Char('Engine Oil - Car (Oli Mobil)')
    auto_car_lubricant_engine_oil_car_qty = fields.Float('Engine Oil - Car (Oli Mobil) (Total Unit)')

    auto_car_lubricant_automatic_transmission_fluid_shell = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Shell)')
    auto_car_lubricant_automatic_transmission_fluid_top1 = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Top 1)')
    auto_car_lubricant_automatic_transmission_fluid_castrol = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Castrol)')
    auto_car_lubricant_automatic_transmission_fluid_pertamina = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Pertamina)')
    auto_car_lubricant_automatic_transmission_fluid_motul = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Motul)')
    auto_car_lubricant_automatic_transmission_fluid_repsol = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Repsol)')
    auto_car_lubricant_automatic_transmission_fluid_mobil = fields.Boolean('Automatic Transmission Fluid (ATF) - Car (Mobil)')
    auto_car_lubricant_automatic_transmission_fluid_others = fields.Char('Automatic Transmission Fluid (ATF) - Car')
    auto_car_lubricant_automatic_transmission_fluid_qty = fields.Float('Automatic Transmission Fluid (ATF) - Car (Total Unit)')
    
    # AUTO - MOTOR LUBRICANT
    auto_motor_lubricant_engine_oil_motorcycle_shell = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Shell)')
    auto_motor_lubricant_engine_oil_motorcycle_top1 = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Top 1)')
    auto_motor_lubricant_engine_oil_motorcycle_castrol = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Castrol)')
    auto_motor_lubricant_engine_oil_motorcycle_pertamina = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Pertamina)')
    auto_motor_lubricant_engine_oil_motorcycle_federal = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Federal)')
    auto_motor_lubricant_engine_oil_motorcycle_yamalube = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (Yamalube)')
    auto_motor_lubricant_engine_oil_motorcycle_ahm = fields.Boolean('Engine Oil - Motor Cycle (Oli Motor) (AHM)')
    auto_motor_lubricant_engine_oil_motorcycle_others = fields.Char('Engine Oil - Motor Cycle (Oli Motor)')
    auto_motor_lubricant_engine_oil_motorcycle_qty = fields.Float('Engine Oil - Motor Cycle (Oli Motor) (Total Unit)')

    auto_motor_lubricant_automatic_transmission_fluid_shell = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Shell)')
    auto_motor_lubricant_automatic_transmission_fluid_top1 = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Top 1)')
    auto_motor_lubricant_automatic_transmission_fluid_castrol = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Castrol)')
    auto_motor_lubricant_automatic_transmission_fluid_pertamina = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Pertamina)')
    auto_motor_lubricant_automatic_transmission_fluid_federal = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Federal)')
    auto_motor_lubricant_automatic_transmission_fluid_yamalube = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (Yamalube)')
    auto_motor_lubricant_automatic_transmission_fluid_ahm = fields.Boolean('Automatic Transmission Fluid (ATF) - Motorcycle (AHM)')
    auto_motor_lubricant_automatic_transmission_fluid_others = fields.Char('Automatic Transmission Fluid (ATF) - Motorcycle')
    auto_motor_lubricant_automatic_transmission_fluid_qty = fields.Float('Automatic Transmission Fluid (ATF) - Motorcycle (Total Unit)')

    auto_motor_lubricant_accu = fields.Boolean('Accu (Total Unit)')
    auto_motor_lubricant_mesin_spooring = fields.Boolean('Mesin Spooring (Total Unit)')
    auto_motor_lubricant_sparepart = fields.Boolean('Spare Part (Total Unit)')
    auto_motor_lubricant_others = fields.Boolean('Others (Total Unit)')

    # PHOTO
    photo_digital_still_camera_compact_canon = fields.Boolean('Digital Still Camera - Compact (Canon)')
    photo_digital_still_camera_compact_nikon = fields.Boolean('Digital Still Camera - Compact (Nikon)')
    photo_digital_still_camera_compact_samsung = fields.Boolean('Digital Still Camera - Compact (Samsung)')
    photo_digital_still_camera_compact_olympus = fields.Boolean('Digital Still Camera - Compact (Olympus)')
    photo_digital_still_camera_compact_fujifilm = fields.Boolean('Digital Still Camera - Compact (Fujifilm)')
    photo_digital_still_camera_compact_tamron = fields.Boolean('Digital Still Camera - Compact (Tamron)')
    photo_digital_still_camera_compact_sony = fields.Boolean('Digital Still Camera - Compact (Sony)')
    photo_digital_still_camera_compact_others = fields.Char('Digital Still Camera - Compact')
    photo_digital_still_camera_compact_qty = fields.Float('Digital Still Camera - Compact (Total Unit)')

    photo_digital_still_camera_slr_canon = fields.Boolean('Digital Still Camera - SLR (Canon)')
    photo_digital_still_camera_slr_nikon = fields.Boolean('Digital Still Camera - SLR (Nikon)')
    photo_digital_still_camera_slr_samsung = fields.Boolean('Digital Still Camera - SLR (Samsung)')
    photo_digital_still_camera_slr_olympus = fields.Boolean('Digital Still Camera - SLR (Olympus)')
    photo_digital_still_camera_slr_fujifilm = fields.Boolean('Digital Still Camera - SLR (Fujifilm)')
    photo_digital_still_camera_slr_tamron = fields.Boolean('Digital Still Camera - SLR (Tamron)')
    photo_digital_still_camera_slr_sony = fields.Boolean('Digital Still Camera - SLR (Sony)')
    photo_digital_still_camera_slr_others = fields.Char('Digital Still Camera - SLR')
    photo_digital_still_camera_slr_qty = fields.Float('Digital Still Camera - SLR (Total Unit)')

    photo_digital_still_camera_mirrorless_canon = fields.Boolean('Digital Still Camera - Mirrorless (Canon)')
    photo_digital_still_camera_mirrorless_nikon = fields.Boolean('Digital Still Camera - Mirrorless (Nikon)')
    photo_digital_still_camera_mirrorless_samsung = fields.Boolean('Digital Still Camera - Mirrorless (Samsung)')
    photo_digital_still_camera_mirrorless_olympus = fields.Boolean('Digital Still Camera - Mirrorless (Olympus)')
    photo_digital_still_camera_mirrorless_fujifilm = fields.Boolean('Digital Still Camera - Mirrorless (Fujifilm)')
    photo_digital_still_camera_mirrorless_tamron = fields.Boolean('Digital Still Camera - Mirrorless (Tamron)')
    photo_digital_still_camera_mirrorless_sony = fields.Boolean('Digital Still Camera - Mirrorless (Sony)')
    photo_digital_still_camera_mirrorless_others = fields.Char('Digital Still Camera - Mirrorless')
    photo_digital_still_camera_mirrorless_qty = fields.Float('Digital Still Camera - Mirrorless (Total Unit)')

    photo_lens_for_slr_canon = fields.Boolean('Lens for SLR (Lensa SLR) (Canon)')
    photo_lens_for_slr_nikon = fields.Boolean('Lens for SLR (Lensa SLR) (Nikon)')
    photo_lens_for_slr_samsung = fields.Boolean('Lens for SLR (Lensa SLR) (Samsung)')
    photo_lens_for_slr_olympus = fields.Boolean('Lens for SLR (Lensa SLR) (Olympus)')
    photo_lens_for_slr_fujifilm = fields.Boolean('Lens for SLR (Lensa SLR) (Fujifilm)')
    photo_lens_for_slr_tamron = fields.Boolean('Lens for SLR (Lensa SLR) (Tamron)')
    photo_lens_for_slr_sony = fields.Boolean('Lens for SLR (Lensa SLR) (Sony)')
    photo_lens_for_slr_others = fields.Char('Lens for SLR (Lensa SLR)')
    photo_lens_for_slr_qty = fields.Float('Lens for SLR (Lensa SLR) (Total Unit)')

    photo_mesin_cuci_cetak = fields.Boolean('Mesin Cuci Cetak (Total Unit)')
    photo_photo_accessories = fields.Boolean('Photo Accessories (Total Unit)')
    photo_others = fields.Boolean('Others (Total Unit)')

    # IT (DPC/PPC) & Peripheral
    it_peripheral_branded_desktop_pc_hp = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (HP)')
    it_peripheral_branded_desktop_pc_acer = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (Acer)')
    it_peripheral_branded_desktop_pc_samsung = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (Samsung)')
    it_peripheral_branded_desktop_pc_advan = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (Advan)')
    it_peripheral_branded_desktop_pc_lenovo = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (Lenovo)')
    it_peripheral_branded_desktop_pc_asus = fields.Boolean('Branded Desktop PC (Komputer ber-merek) (Asus)')
    it_peripheral_branded_desktop_pc_others = fields.Char('Branded Desktop PC (Komputer ber-merek)')
    it_peripheral_branded_desktop_pc_qty = fields.Float('Branded Desktop PC (Komputer ber-merek) (Total Unit)')

    it_peripheral_portable_pc_hp = fields.Boolean('Portable PC (Laptop/Notebook) (HP)')
    it_peripheral_portable_pc_acer = fields.Boolean('Portable PC (Laptop/Notebook) (Acer)')
    it_peripheral_portable_pc_samsung = fields.Boolean('Portable PC (Laptop/Notebook) (Samsung)')
    it_peripheral_portable_pc_advan = fields.Boolean('Portable PC (Laptop/Notebook) (Advan)')
    it_peripheral_portable_pc_lenovo = fields.Boolean('Portable PC (Laptop/Notebook) (Lenovo)')
    it_peripheral_portable_pc_asus = fields.Boolean('Portable PC (Laptop/Notebook) (Asus)')
    it_peripheral_portable_pc_others = fields.Char('Portable PC (Laptop/Notebook)')
    it_peripheral_portable_pc_qty = fields.Float('Portable PC (Laptop/Notebook) (Total Unit)')

    it_peripheral_monitor = fields.Float('Monitor [IT Peripherals] (Total Unit)')
    it_peripheral_assembled_desktop_pc = fields.Boolean('Assembled Desktop PC (Komputer Rakitan) (Total Unit)')

    # IT Peripherals
    it_peripherals_printer_canon = fields.Boolean('Printer/MFD (Multi Fungsi) (Canon)')
    it_peripherals_printer_hp = fields.Boolean('Printer/MFD (Multi Fungsi) (HP)')
    it_peripherals_printer_fujixerox = fields.Boolean('Printer/MFD (Multi Fungsi) (Fuji Xerox)')
    it_peripherals_printer_panasonic = fields.Boolean('Printer/MFD (Multi Fungsi) (Panasonic)')
    it_peripherals_printer_brother = fields.Boolean('Printer/MFD (Multi Fungsi) (Brother)')
    it_peripherals_printer_epson = fields.Boolean('Printer/MFD (Multi Fungsi) (Epson)')
    it_peripherals_printer_others = fields.Char('Printer/MFD (Multi Fungsi)')
    it_peripherals_printer_qty = fields.Float('Printer/MFD (Multi Fungsi) (Total Unit)')

    # IT Components
    it_components_motherboards_vga_processor = fields.Boolean('Motherboard/VGA/Processor (Display)')
    it_components_memory_ram = fields.Boolean('Memory/RAM (Display)')
    it_components_harddisc_dvdcd = fields.Boolean('Hard Disc Int., DVD/CD-Rom (Display)')
    it_components_others = fields.Boolean('Others (Display)')

    # IT Networking
    it_networking_routers = fields.Boolean('Routers (Display)')
    it_networking_usbmodem = fields.Boolean('USB Modem (Mobile broadband) (Display)')
    it_networking_comm_device = fields.Boolean('Comm. Device (Display)')
    it_networking_others = fields.Boolean('Others (Display)')

    # IT Accessories
    it_accessories_keyboard_mouse_votre = fields.Boolean('Keyboard, Mouse [IT Acc] (Votre)')
    it_accessories_keyboard_mouse_genius = fields.Boolean('Keyboard, Mouse [IT Acc] (Genius)')
    it_accessories_keyboard_mouse_logitech = fields.Boolean('Keyboard, Mouse [IT Acc] (Logitech)')
    it_accessories_keyboard_mouse_westerndigital = fields.Boolean('Keyboard, Mouse [IT Acc] (Western Digital)')
    it_accessories_keyboard_mouse_eyota = fields.Boolean('Keyboard, Mouse [IT Acc] (Eyota)')
    it_accessories_keyboard_mouse_toshiba = fields.Boolean('Keyboard, Mouse [IT Acc] (Toshiba)')
    it_accessories_keyboard_mouse_seagate = fields.Boolean('Keyboard, Mouse [IT Acc] (Seagate)')
    it_accessories_keyboard_mouse_others = fields.Char('Keyboard, Mouse [IT Acc]')
    it_accessories_keyboard_mouse_qty = fields.Float('Keyboard, Mouse [IT Acc] (Total Unit)')

    it_accessories_harddisk_external_votre = fields.Boolean('Hard Disk External [IT Acc] (Votre)')
    it_accessories_harddisk_external_genius = fields.Boolean('Hard Disk External [IT Acc] (Genius)')
    it_accessories_harddisk_external_logitech = fields.Boolean('Hard Disk External [IT Acc] (Logitech)')
    it_accessories_harddisk_external_westerndigital = fields.Boolean('Hard Disk External [IT Acc] (Western Digital)')
    it_accessories_harddisk_external_eyota = fields.Boolean('Hard Disk External [IT Acc] (Eyota)')
    it_accessories_harddisk_external_toshiba = fields.Boolean('Hard Disk External [IT Acc] (Toshiba)')
    it_accessories_harddisk_external_seagate = fields.Boolean('Hard Disk External [IT Acc] (Seagate)')
    it_accessories_harddisk_external_others = fields.Char('Hard Disk External [IT Acc]')
    it_accessories_harddisk_external_qty = fields.Float('Hard Disk External [IT Acc] (Total Unit)')

    it_accessories_cartridge = fields.Boolean('Cartridge (Tinta/Toner) [IT Acc] (Total Unit)')
    it_accessories_minispeaker = fields.Boolean('Mini Speaker (untuk PC/MP3) [IT Acc] (Total Unit)')
    it_accessories_Software = fields.Boolean('Software [IT Acc] (Total Unit)')
    it_accessories_others = fields.Boolean('Others (Total Unit)')

    # IT Accessories & Telecom
    it_accessories_telecom_usbfd_vgen = fields.Boolean('USB Flash Disk [IT Acc] (V-Gen)')
    it_accessories_telecom_usbfd_hp = fields.Boolean('USB Flash Disk [IT Acc] (HP)')
    it_accessories_telecom_usbfd_kingston = fields.Boolean('USB Flash Disk [IT Acc] (Kingston)')
    it_accessories_telecom_usbfd_sandisk = fields.Boolean('USB Flash Disk [IT Acc] (Sandisk)')
    it_accessories_telecom_usbfd_toshiba = fields.Boolean('USB Flash Disk [IT Acc] (Toshiba)')
    it_accessories_telecom_usbfd_advance = fields.Boolean('USB Flash Disk [IT Acc] (Advance)')
    it_accessories_telecom_usbfd_adata = fields.Boolean('USB Flash Disk [IT Acc] (Adata)')
    it_accessories_telecom_usbfd_others = fields.Char('USB Flash Disk [IT Acc]')
    it_accessories_telecom_usbfd_qty = fields.Float('USB Flash Disk [IT Acc] (Total Unit)')

    it_accessories_telecom_memorycard_vgen = fields.Boolean('Memory Card [IT Acc] (V-Gen)')
    it_accessories_telecom_memorycard_hp = fields.Boolean('Memory Card [IT Acc] (HP)')
    it_accessories_telecom_memorycard_kingston = fields.Boolean('Memory Card [IT Acc] (Kingston)')
    it_accessories_telecom_memorycard_sandisk = fields.Boolean('Memory Card [IT Acc] (Sandisk)')
    it_accessories_telecom_memorycard_toshiba = fields.Boolean('Memory Card [IT Acc] (Toshiba)')
    it_accessories_telecom_memorycard_advance = fields.Boolean('Memory Card [IT Acc] (Advance)')
    it_accessories_telecom_memorycard_adata = fields.Boolean('Memory Card [IT Acc] (Adata)')
    it_accessories_telecom_memorycard_others = fields.Char('Memory Card [IT Acc]')
    it_accessories_telecom_memorycard_qty = fields.Float('Memory Card [IT Acc] (Total Unit)')

    # TELECOM
    telecom_mobile_new_oppo = fields.Boolean('Mobile/Smartphone (new) (Oppo)')
    telecom_mobile_new_mi = fields.Boolean('Mobile/Smartphone (new) (Mi)')
    telecom_mobile_new_lenovo = fields.Boolean('Mobile/Smartphone (new) (Lenovo)')
    telecom_mobile_new_samsung = fields.Boolean('Mobile/Smartphone (new) (Samsung)')
    telecom_mobile_new_advan = fields.Boolean('Mobile/Smartphone (new) (Advan)')
    telecom_mobile_new_nokia = fields.Boolean('Mobile/Smartphone (new) (Nokia)')
    telecom_mobile_new_asus = fields.Boolean('Mobile/Smartphone (new) (Asus)')
    telecom_mobile_new_others = fields.Boolean('Mobile/Smartphone (new)')
    telecom_mobile_new_qty = fields.Float('Mobile/Smartphone (new) (Total Unit)')

    telecom_webbook_tablet = fields.Boolean('Webbook/Tablet (Total Unit)')

    telecom_headset_handsfree_bluetooth_oppo = fields.Boolean('Headset/Handsfree/Bluetooth (Oppo)')
    telecom_headset_handsfree_bluetooth_mi = fields.Boolean('Headset/Handsfree/Bluetooth (Mi)')
    telecom_headset_handsfree_bluetooth_lenovo = fields.Boolean('Headset/Handsfree/Bluetooth (Lenovo)')
    telecom_headset_handsfree_bluetooth_samsung = fields.Boolean('Headset/Handsfree/Bluetooth (Samsung)')
    telecom_headset_handsfree_bluetooth_advan = fields.Boolean('Headset/Handsfree/Bluetooth (Advan)')
    telecom_headset_handsfree_bluetooth_nokia = fields.Boolean('Headset/Handsfree/Bluetooth (Nokia)')
    telecom_headset_handsfree_bluetooth_asus = fields.Boolean('Headset/Handsfree/Bluetooth (Asus)')
    telecom_headset_handsfree_bluetooth_others = fields.Boolean('Headset/Handsfree/Bluetooth')
    telecom_headset_handsfree_bluetooth_qty = fields.Float('Headset/Handsfree/Bluetooth (Total Unit)')

    telecom_mobile_second = fields.Boolean('Mobile/Smartphone (2nd hand) (Total Unit)')
    telecom_mobile_tariff_kartu = fields.Boolean('Mobile Tariff (Kartu Perdana/Voucher) (Total Unit)')
    telecom_telecom_accessories = fields.Boolean('Telecom Accessories (Total Unit)')
    telecom_others = fields.Boolean('Others (Total Unit)')

    # Home Entertainment
    home_entertainment_cddvd_music_film = fields.Boolean('CD/DVD Music/Film (Display)')
    home_entertainment_videogame_console = fields.Float('Video Game Console (player) (Display)')
    home_entertainment_videogame_software = fields.Boolean('Video Game Software (CD Game) (Display)')
    home_entertainment_hevgc_accessories = fields.Boolean('HE/VGC Accessories (Display)')
    home_entertainment_others = fields.Boolean('Others (Display)')

    # Others
    others_others = fields.Boolean('Others (Display)')
    
    